## FIOT

The FIOT project is exploring the requirements and affordances of inexpensive, connected electronics in an agroecology and laboratory context.

## Architectures

In general, one needs at least a microcontroller (uC), or a single board computer, or some combination of the two to get signals into accessible data locations (datalogging, a local computer, a remote server).  

Microcontrollers are simpler devices, and less likely to fail unexpectedly; for the simpler measurement / actuation tasks (acquiring a temperature and storing it locally, or broadcasting it over wifi), a single microcontroller may provide an advantage in increased reliability and decreased cost and complexity.  For more complex tasks, a single board computer (SBC) like the Raspberry Pi, with its associated increased capability (and cost and complexity) may be required. 

## Notes on Microcontroller <--> SBC communication

In some cases, it is an advantage to use a uC for direct measurement / actuation tasks, as well as a SBC to interface with the mCs in order to gather data, perform control and analysis, accomplish networking.  Here, there are various choices of protocol and physical connection to connect the SBC to the uC: USB, i2c, SPI, etc.  Of these approaches, i2c and USB are currently the most common in the hobby microelectronics world -- i2c being used by the GrovePi system, and USB being used by a typical Arduino-connected-to-laptop setup. USB has the advantage of being, in general, less hardware-specific;  a USB-based protocol will in principle work on any computer (including a PC or laptop) that has a USB port (and the proper drivers), while i2c and SPI are specific to particular SBC implementations (the Raspberry Pi and the Beagle Bone implement i2c differently, on different pins; laptops and PCs typically do not provide access to i2c).  Further, USB communication can typically be accomplished with easy-to-interpret serial commands ('TEMP?', 'ON!') that are readily debugged; i2c communication happens at the byte level.  i2c firmware is harder to write and interpret, as it must obey a particular clock speed for communication, requiring slower sensors and actuators to provide their information across several 'loop cycles'. USB communication has the further advantage that higher data transmission rates are typically possible than i2c.  
